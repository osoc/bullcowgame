#include "BullCowCartridge.h"

void UBullCowCartridge::BeginPlay() // When the game starts
{
	Super::BeginPlay();
	SetUpGame();
}

void UBullCowCartridge::OnInput(const FString& Input) // When the player hits enter
{
	if (Input.ToLower() != "y" && bGameOver)
	{
		FGenericPlatformMisc::RequestExit(false); // Quits the game (and the editor, so be careful!)
	}
	else if (bGameOver)
	{
		SetUpGame();
	}
	else 
	{
		if (Input == HiddenWord)
		{
			PrintLine(TEXT("Well Done!"));
			PrintLine(TEXT("You guessed the correct Isogram!"));
			EndGame();
		}
		else
		{
			if (Input.Len() != WordLength)
			{
				PrintLine(TEXT("Your guess is not %i characters long!"), WordLength);
			}
			else
			{
				PrintLine(TEXT("Oh no..."));
				PrintLine(TEXT("You got it wrong."));
				--Lives;
			}
		}
		// @todo Check if lives == 0
		// @todo If yes ask if the player wishes to Guess Again
		// @todo Show lives left

		// @todo Check if Input is an isogram
	}
}

void UBullCowCartridge::SetUpGame()
{
	ClearScreen();
	bGameOver = false;
	HiddenWord = TEXT("beans"); // Set the HiddenWord
	WordLength = HiddenWord.Len();
	Lives = WordLength;

	PrintLine(TEXT("The hidden word is %s."), *HiddenWord); // Debug

	PrintLine(TEXT("Welcome to Bulls and Cows!"));
	PrintLine(TEXT("We want you to guess a %i letter word."), WordLength);
	PrintLine(TEXT("You have %i lives."), Lives);
	PrintLine(TEXT("If you fail... You DIE."));
	PrintLine(TEXT("Good Luck! :)"));
	PrintLine(TEXT("Type in your guess and press enter to\ncontinue..."));
}

/**
 * Function to set bGameOver to true, and ask the player if they want to play again
 */
void UBullCowCartridge::EndGame()
{
	bGameOver = true;
	PrintLine(TEXT("The game is now over."));
	PrintLine(TEXT("Would you like to play again?"));
	PrintLine("Enter Y or N and press enter");
}
